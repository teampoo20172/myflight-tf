package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GerenciadorAeronaves {

	private Map<String, Aeronave> aeronaves;

	public GerenciadorAeronaves() {
		aeronaves = new HashMap<String, Aeronave>();
	}

	public void adicionar(Aeronave a) {
		aeronaves.put(a.getCodigo(), a);
	}


	public static void ordenarDescricao(ArrayList<Aeronave> array_aeronaves) {
		Collections.sort(array_aeronaves);
		array_aeronaves.sort( (Aeronave a1, Aeronave a2)
				-> a1.getDescricao().compareTo(a2.getDescricao()));
		array_aeronaves.sort(Comparator.comparing(a -> a.getDescricao()));
		array_aeronaves.sort(Comparator.comparing(Aeronave::getDescricao));
	}
	
	public static void ordenarCodigo(ArrayList<Aeronave> array_aeronaves) {
		Collections.sort(array_aeronaves);
		array_aeronaves.sort( (Aeronave a1, Aeronave a2)
				-> a1.getDescricao().compareTo(a2.getCodigo()));
		array_aeronaves.sort(Comparator.comparing(a -> a.getCodigo()));
		array_aeronaves.sort(Comparator.comparing(Aeronave::getCodigo));
	}

	public ArrayList<Aeronave> toArray() {
		ArrayList<Aeronave> temp_avioes = new ArrayList<Aeronave>();
		for(String key : aeronaves.keySet()) {
			temp_avioes.add(aeronaves.get(key));
		}
		return temp_avioes;
	}
	
	public ArrayList<Aeronave> toArrayOrdenado() {
		ArrayList<Aeronave> temp_avioes = new ArrayList<Aeronave>();
		for(String key : aeronaves.keySet()) {
			temp_avioes.add(aeronaves.get(key));
		}
		ordenarCodigo(temp_avioes);
		return temp_avioes;
	}
	
	public Aeronave buscarCodigo(String cod) {
		return aeronaves.get(cod);
	}
	
	public void carregarAeronaves() throws IOException {
		//System.out.println("### Carregando Aeronaves ###\n\n");
		Path path2 = Paths.get("equipment.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path2, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
			String header = sc.nextLine(); // pula cabeçalho
			String id, desc;
			int capacidade;
			while (sc.hasNext()) {
				id = sc.next();
				desc = sc.next();
				capacidade = Integer.parseInt(sc.next().replaceAll("(\r)", "")); // incrementado devido a novo arquivo, alterado construtor
				Aeronave a = new Aeronave(id, desc, capacidade);
				adicionar(a);
				//System.out.format("%s - %s - %s (%s)", id, desc, capacidade, a.getCodigo());
			}
		}
		catch (IOException x) {
			System.err.format("Erro de E/S: %s%n", x);
		}
		//System.out.println("### Encerrado Carregamento Aeronaves ###\n\n");
	}
}
