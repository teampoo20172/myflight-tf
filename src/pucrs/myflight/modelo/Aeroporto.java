package pucrs.myflight.modelo;

public class Aeroporto implements Comparable<Aeroporto> {
	private String codigo;
	private String nome;
	private Geo loc;
	private String cod_pais; // Incrementado
	
	public Aeroporto(String codigo, String nome, Geo loc, String cod_pais) {
		this.codigo = codigo;
		this.nome = nome;
		this.loc = loc;
		this.cod_pais = cod_pais;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Geo getLocal() {
		return loc;
	}
	
	public String getCodigoPais() {
		return cod_pais;
	}
	
	@Override
	public String toString() {
		return String.format("(%S): %s", codigo, nome);
	}
	
	public String describe () {
		return String.format("%s - %s - %s - %s", codigo, nome, loc, cod_pais );
	}

	@Override
	public int compareTo(Aeroporto o) {
		return nome.compareTo(o.nome);
	}
}
