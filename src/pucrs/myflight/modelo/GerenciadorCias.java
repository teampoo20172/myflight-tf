package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GerenciadorCias {
	private Map<String, CiaAerea> empresas;
	
	public GerenciadorCias() {
		empresas = new HashMap<String, CiaAerea>();
	}
	
	public void adicionar(CiaAerea nova) {
		empresas.put(nova.getCodigo(), nova);
	}
	
	public void carregaCias() throws IOException {
		//System.out.println("### Carregando Companhias ###");
		Path path2 = Paths.get("airlines.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path2, Charset.forName("utf8")))) {
		  sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
		  String header = sc.nextLine(); // pula cabeçalho
		  String codigo, nome;
		  while (sc.hasNext()) {
		    codigo = sc.next();
		    nome = sc.next().replaceAll("(\r)", "");
		    CiaAerea nova = new CiaAerea(codigo, nome);
		    adicionar(nova);
		    //System.out.format("%s - %s", codigo, nome);
		  }
		}
		catch (IOException x) {
		  System.err.format("Erro de E/S: %s%n", x);
		}
		//System.out.println("### Encerrado Carregamento Companhias ###\n\n");
	}
	
	public ArrayList<CiaAerea> toArray() {
		ArrayList<CiaAerea> temp_empresas = new ArrayList<CiaAerea>();
		for(String key : empresas.keySet()) {
			temp_empresas.add(empresas.get(key));
		}
		return temp_empresas;
	}
	
	public CiaAerea buscarCodigo(String cod) {
		return empresas.get(cod);
	}
	
	public CiaAerea buscarNome(String nome) {
		ArrayList<CiaAerea> temp = toArray();
		for(CiaAerea cia : temp) {
			if(nome.equals(cia.getNome())) {
				return cia;
			}
		}
		return null;
	}
	
//	public CiaAerea buscarCodigo(String cod) {
//		for(CiaAerea cia: empresas)
//			if(cod.equals(cia.getCodigo()))
//				return cia;
//		return null; // Não achei!
//	}
//	
//	public CiaAerea buscarNome(String nome) {
//		for(CiaAerea cia: empresas)
//			if(nome.equals(cia.getNome()))
//				return cia;
//		return null; // Não achei!
//	}
}
