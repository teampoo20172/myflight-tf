package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class GerenciadorRotas {

	private ArrayList<Rota> rotas;

	public GerenciadorRotas() {
		rotas = new ArrayList<>();
	}

	public void adicionar(Rota r) {
		rotas.add(r);
	}

	public void ordenarCias() {
		// Collections.sort(rotas);
		rotas.sort((Rota r1, Rota r2) -> r1.getCia().getNome().compareTo(r2.getCia().getNome()));
	}

	public void ordenarOrigem() {
		rotas.sort((Rota r1, Rota r2) -> r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome()));
	}

	public void ordenarOrigemCias() {
		/*
		rotas.sort((Rota r1, Rota r2) -> {
			int res = r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome());
			if (res != 0) // não empatou no nome da origem...
				return res;
			// empatou, desempata pelo nome da cia.
			return r1.getCia().getNome().compareTo(r2.getCia().getNome());
		});
		*/
		rotas.sort(Comparator.comparing(
				(Rota r) -> r.getOrigem().getNome())
				.thenComparing((Rota r) -> r.getCia().getNome()));
	}

	public ArrayList<Rota> toArray() {
		return rotas;
	}
	
	public void carregaRotas(GerenciadorCias gerCias, GerenciadorAeroportos gerAero, GerenciadorAeronaves gerAvioes) throws IOException {
		//System.out.println("### Carregando Rotas ###\n\n");
		Path path2 = Paths.get("routes.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path2, Charset.forName("utf8")))) {
		  sc.useDelimiter("[;\n ]"); // separadores: ; e nova linha
		  String header = sc.nextLine(); // pula cabeçalho
		  String s_cia, s_ori, s_dest, s_equip;
		  int linha = 1;
		  while (sc.hasNext()) {
		    s_cia = sc.next().replaceAll("(\r)", "");
		    s_ori = sc.next().replaceAll("(\r)", "");
		    s_dest = sc.next().replaceAll("(\r)", "");
		    sc.next(); // codeshare
		    sc.next(); // stops
		    s_equip = sc.next().replaceAll("(\r)", "");
		    sc.nextLine(); linha++;
		    //System.out.println(s_cia + " - " + s_ori +  " - " + s_dest +  " - " + s_equip);
		    CiaAerea cia = gerCias.buscarCodigo(s_cia);
		    Aeroporto origem = gerAero.buscarCodigo(s_ori);
		    Aeroporto destino = gerAero.buscarCodigo(s_dest);
		    Aeronave aeronave = gerAvioes.buscarCodigo(s_equip);
		    //System.out.format("(%s): %s - %s - %s - %s%n", linha, cia, origem, destino, aeronave);
		    Rota r = new Rota(cia, origem, destino, aeronave);
		    adicionar(r);
		  }
		}
		catch (IOException x) {
		  System.err.format("Erro de E/S: %s%n", x);
		}
		//System.out.println("### Encerrado Carregento de Rotas ###\n\n");
	}

}
