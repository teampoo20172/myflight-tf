package pucrs.myflight.gui;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import javax.swing.SwingUtilities;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;
import grafos.dijkstra.*;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingNode;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import pucrs.myflight.modelo.*;

public class JanelaFX extends Application {

	final SwingNode mapkit = new SwingNode();

	private GerenciadorPaises gerPaises; // ok
	private GerenciadorCias gerCias; // ok
	private GerenciadorAeroportos gerAero; // ok
	private GerenciadorRotas gerRotas;
	private GerenciadorAeronaves gerAvioes;

	private GerenciadorMapa gerenciador;

	private EventosMouse mouse;
	
	private BorderPane pane;
	private TextArea output;
	
	private double defaultToolboxItemWidth = 250.0;
	private double defaultToolboxItemHeight = 20.0;

	@Override
	public void start(Stage primaryStage) throws Exception {

		setup();

		GeoPosition poa = new GeoPosition(-30.05, -51.18);
		gerenciador = new GerenciadorMapa(poa, GerenciadorMapa.FonteImagens.VirtualEarth);
		mouse = new EventosMouse();
		gerenciador.getMapKit().getMainMap().addMouseListener(mouse);
		gerenciador.getMapKit().getMainMap().addMouseMotionListener(mouse);

		createSwingContent(mapkit);

		setupUI(); // interface gr�fica
		gerenciador.setMaxZoomText(11);
		
		Scene scene = new Scene(pane, 800, 640);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Mapas com JavaFX");
		primaryStage.show();
	}

	// Inicializando os dados aqui...
	private void setup() {

		gerPaises = new GerenciadorPaises();
		try {
			gerPaises.carregaPaises();
		} catch (IOException e) {
			System.err.println("N�o foi poss�vel carregar banco de dados de Companhias");
		}

		gerCias = new GerenciadorCias();
		try {
			gerCias.carregaCias();
		} catch (IOException e) {
			System.err.println("N�o foi poss�vel carregar banco de dados de Companhias");
		}

		gerAero = new GerenciadorAeroportos();
		try {
			gerAero.carregaAeroportos();
		} catch (IOException e) {
			System.err.println("N�o foi poss�vel carregar banco de dados de Aeroportos");
		}

		gerPaises.carregaAeroportos(gerAero); // Carrega Aeroportos p/ dentro de seus paises

		gerAvioes = new GerenciadorAeronaves();
		try {
			gerAvioes.carregarAeronaves();
		} catch (IOException e) {
			System.err.println("N�o foi poss�vel carregar banco de dados de Aeroportos");
		}

		gerRotas = new GerenciadorRotas();
		try {
			gerRotas.carregaRotas(gerCias, gerAero, gerAvioes);
		} catch (IOException e) {
			System.err.println("N�o foi poss�vel carregar banco de dados de Aeroportos");
		}
		//System.out.println("\n\nEncerrado Carregamento dos dados de arquivos. . .");
	}
	
	
	public void setupUI() {
		pane = new BorderPane();
		pane.setPadding(new Insets(3, 3, 3, 3));
		
		Label labelConsulta1 = new Label();
		labelConsulta1.setMinHeight(defaultToolboxItemHeight);
		labelConsulta1.setAlignment(Pos.CENTER_LEFT);
		labelConsulta1.setMinWidth(defaultToolboxItemWidth);
		labelConsulta1.setPrefWidth(defaultToolboxItemWidth);
		labelConsulta1.setMaxWidth(defaultToolboxItemWidth);
		Button btnConsulta1 = new Button("Consultar");
		btnConsulta1.setMinWidth(defaultToolboxItemWidth);
		btnConsulta1.setPrefWidth(defaultToolboxItemWidth);
		btnConsulta1.setMaxWidth(defaultToolboxItemWidth);
		
		Label labelConsulta2 = new Label();
		labelConsulta2.setMinHeight(defaultToolboxItemHeight);
		labelConsulta2.setAlignment(Pos.CENTER_LEFT);
		labelConsulta2.setMinWidth(defaultToolboxItemWidth);
		labelConsulta2.setPrefWidth(defaultToolboxItemWidth);
		labelConsulta2.setMaxWidth(defaultToolboxItemWidth);
		Button btnConsulta2 = new Button("Consultar");
		btnConsulta2.setMinWidth(defaultToolboxItemWidth);
		btnConsulta2.setPrefWidth(defaultToolboxItemWidth);
		btnConsulta2.setMaxWidth(defaultToolboxItemWidth);
		
		Label labelConsulta3 = new Label();
		labelConsulta3.setMinHeight(defaultToolboxItemHeight);
		labelConsulta3.setAlignment(Pos.CENTER_LEFT);
		labelConsulta3.setMinWidth(defaultToolboxItemWidth);
		labelConsulta3.setPrefWidth(defaultToolboxItemWidth);
		labelConsulta3.setMaxWidth(defaultToolboxItemWidth);
		Button btnConsulta3 = new Button("Consultar");
		btnConsulta3.setMinWidth(defaultToolboxItemWidth*1/3);
		btnConsulta3.setPrefWidth(defaultToolboxItemWidth*1/3);
		btnConsulta3.setMaxWidth(defaultToolboxItemWidth*1/3);
		Spinner<Double> spinnerConsulta3 = new Spinner<>(0.0, Double.MAX_VALUE, 500.0, 100.0);
		spinnerConsulta3.setMinWidth(defaultToolboxItemWidth*2/3);
		spinnerConsulta3.setPrefWidth(defaultToolboxItemWidth*2/3);
		spinnerConsulta3.setMaxWidth(defaultToolboxItemWidth*2/3);
		spinnerConsulta3.setEditable(true);
		
		Label labelConsulta4 = new Label();
		labelConsulta4.setMinHeight(defaultToolboxItemHeight);
		labelConsulta4.setAlignment(Pos.CENTER_LEFT);
		labelConsulta4.setMinWidth(defaultToolboxItemWidth);
		labelConsulta4.setPrefWidth(defaultToolboxItemWidth);
		labelConsulta4.setMaxWidth(defaultToolboxItemWidth);
		Button btnConsulta4 = new Button("Consultar");
		btnConsulta4.setMinWidth(defaultToolboxItemWidth*1/3);
		btnConsulta4.setPrefWidth(defaultToolboxItemWidth*1/3);
		btnConsulta4.setMaxWidth(defaultToolboxItemWidth*1/3);
		ComboBox<Pais> comboConsulta4 = new ComboBox<Pais>();
		comboConsulta4.setMinWidth(defaultToolboxItemWidth*2/3);
		comboConsulta4.setPrefWidth(defaultToolboxItemWidth*2/3);
		comboConsulta4.setMaxWidth(defaultToolboxItemWidth*2/3);
		
		Label labelConsulta5 = new Label();
		labelConsulta5.setMinHeight(defaultToolboxItemHeight);
		labelConsulta5.setAlignment(Pos.CENTER_LEFT);
		labelConsulta5.setMinWidth(defaultToolboxItemWidth);
		labelConsulta5.setPrefWidth(defaultToolboxItemWidth);
		labelConsulta5.setMaxWidth(defaultToolboxItemWidth);
		Button btnConsulta5 = new Button("Consultar");
		btnConsulta5.setMinWidth(defaultToolboxItemWidth);
		btnConsulta5.setPrefWidth(defaultToolboxItemWidth);
		btnConsulta5.setMaxWidth(defaultToolboxItemWidth);
		ComboBox<Aeronave> comboConsulta5 = new ComboBox<Aeronave>();
		comboConsulta5.setMinWidth(defaultToolboxItemWidth);
		comboConsulta5.setPrefWidth(defaultToolboxItemWidth);
		comboConsulta5.setMaxWidth(defaultToolboxItemWidth);
		
		// Ponto extra
		Label labelConsulta7 = new Label();
		labelConsulta7.setMinHeight(defaultToolboxItemHeight);
		labelConsulta7.setAlignment(Pos.CENTER_LEFT);
		labelConsulta7.setMinWidth(defaultToolboxItemWidth);
		labelConsulta7.setPrefWidth(defaultToolboxItemWidth);
		labelConsulta7.setMaxWidth(defaultToolboxItemWidth);
		Button btnConsulta7 = new Button("Consultar");
		btnConsulta7.setMinWidth(defaultToolboxItemWidth);
		btnConsulta7.setPrefWidth(defaultToolboxItemWidth);
		btnConsulta7.setMaxWidth(defaultToolboxItemWidth);
		TextField textfieldConsulta7A = new TextField();
		textfieldConsulta7A.setMinWidth(defaultToolboxItemWidth/3);
		textfieldConsulta7A.setPrefWidth(defaultToolboxItemWidth/3);
		textfieldConsulta7A.setMaxWidth(defaultToolboxItemWidth/3);
		TextField textfieldConsulta7B = new TextField();
		textfieldConsulta7B.setMinWidth(defaultToolboxItemWidth/3);
		textfieldConsulta7B.setPrefWidth(defaultToolboxItemWidth/3);
		textfieldConsulta7B.setMaxWidth(defaultToolboxItemWidth/3);
		ComboBox<Aeroporto> comboConsulta7A = new ComboBox<Aeroporto>();
		comboConsulta7A.setMinWidth(defaultToolboxItemWidth/3);
		comboConsulta7A.setPrefWidth(defaultToolboxItemWidth/3);
		comboConsulta7A.setMaxWidth(defaultToolboxItemWidth/3);
		
		
		//
		
		VBox toolbox = new VBox();
		toolbox.setSpacing(10);
		toolbox.setPadding(new Insets(5, 5, 5, 0)); 
		
		labelConsulta1.setText("Aeroportos do Pais Selecionado:");
		toolbox.setSpacing(5);
		toolbox.getChildren().add(labelConsulta1);
		toolbox.getChildren().add(btnConsulta1);
		
		toolbox.getChildren().add(new Separator());
		
		labelConsulta2.setText("Todos Voos do Pais selecionado:");
		toolbox.getChildren().add(labelConsulta2);
		toolbox.getChildren().add(btnConsulta2);

		toolbox.getChildren().add(new Separator());
		
		labelConsulta3.setText("Voos de origem no aeroporto\ne destino no maximo a (km):");
		toolbox.getChildren().add(labelConsulta3);
		HBox hb3 = new HBox();  
		hb3.getChildren().add(spinnerConsulta3);
		hb3.getChildren().add(btnConsulta3);
		toolbox.getChildren().add(hb3);
		
		toolbox.getChildren().add(new Separator());
		
		labelConsulta4.setText("Dez aeroportos com maior trafego:");
		toolbox.getChildren().add(labelConsulta4);
		HBox hb4 = new HBox(); 
		hb4.getChildren().add(comboConsulta4);
		hb4.getChildren().add(btnConsulta4);
		toolbox.getChildren().add(hb4);
		
		toolbox.getChildren().add(new Separator());
		
		labelConsulta5.setText("Todos voos da aeronave:");
		toolbox.getChildren().add(labelConsulta5);
		toolbox.getChildren().add(comboConsulta5);
		toolbox.getChildren().add(btnConsulta5);
		
		toolbox.getChildren().add(new Separator());
		
		labelConsulta7.setText("Procura rota entre dois aeroportos\ne pelos seus respectivos codigos:");
		toolbox.getChildren().add(labelConsulta7);
		HBox hb7labels = new HBox();
		Label labelO = new Label();labelO.setAlignment(Pos.CENTER_LEFT);labelO.setMinWidth(defaultToolboxItemWidth/3);labelO.setPrefWidth(defaultToolboxItemWidth/3); labelO.setMaxWidth(defaultToolboxItemWidth/3);
		Label labelD = new Label();labelD.setAlignment(Pos.CENTER_LEFT);labelD.setMinWidth(defaultToolboxItemWidth/3);labelD.setPrefWidth(defaultToolboxItemWidth/3); labelD.setMaxWidth(defaultToolboxItemWidth/3);
		Label labelC = new Label();labelC.setAlignment(Pos.CENTER_LEFT);labelC.setMinWidth(defaultToolboxItemWidth/3);labelC.setPrefWidth(defaultToolboxItemWidth/3); labelC.setMaxWidth(defaultToolboxItemWidth/3);
		labelO.setText("Origem"); labelD.setText("Destino"); labelC.setText("Lista de (Cod)igos");
		labelO.setFont(Font.font ("Verdana", 9)); labelD.setFont(Font.font ("Verdana", 9)); labelC.setFont(Font.font ("Verdana", 9));
		hb7labels.getChildren().addAll(labelO, labelD, labelC);
		toolbox.getChildren().add(hb7labels);
		HBox hb7 = new HBox();
		hb7.getChildren().add(textfieldConsulta7A);
		hb7.getChildren().add(textfieldConsulta7B);
		hb7.getChildren().add(comboConsulta7A);
		toolbox.getChildren().add(hb7);
		toolbox.getChildren().add(btnConsulta7);
		
		toolbox.getChildren().add(new Separator());
		
		// Output
		
		output = new TextArea();
		output.setText("Nenhuma consulta realizada ainda!");
		output.setMinWidth(defaultToolboxItemWidth);
		output.setPrefWidth(defaultToolboxItemWidth);
		output.setMaxWidth(defaultToolboxItemWidth);
		output.setMinHeight(defaultToolboxItemHeight*7);
		output.setPrefHeight(defaultToolboxItemHeight*30);
		output.setFont(Font.font ("Verdana", 9));
		toolbox.getChildren().add(output);
		
		
		// adiciona elementos � combobox do pa�s
		Pais mundo = new Pais("MUNDO", "Nivel Mundial");
		for(Aeroporto a : gerAero.toArray()) {
			mundo.getAeroportos().add(a);
		}
		comboConsulta4.setItems(FXCollections.observableArrayList(gerPaises.toArrayOrdenado()));
		comboConsulta4.getItems().add(0, mundo);
		comboConsulta4.getSelectionModel().selectFirst();
		
		// adiciona elementos do gerenciador de avi�es ao combobox
		comboConsulta5.setItems(FXCollections.observableArrayList(gerAvioes.toArrayOrdenado()));
		comboConsulta5.getSelectionModel().selectFirst();
		
		// Consulta 7
		textfieldConsulta7A.setText("CNF");
		textfieldConsulta7B.setText("PNZ");
		ArrayList<Aeroporto> aeroPorCodigo = GerenciadorAeroportos.ordenarCodigo((gerAero.toArray()));
		comboConsulta7A.setItems(FXCollections.observableArrayList(aeroPorCodigo));
		comboConsulta7A.getSelectionModel().select(gerAero.buscarCodigo("CNF"));
		

		btnConsulta1.setOnAction(e -> {
			consulta1();
		});

		btnConsulta2.setOnAction(e -> {
			consulta2();
		});
		
		btnConsulta3.setOnAction(e -> {
			consulta3(spinnerConsulta3.getValue());
		});
		
		btnConsulta4.setOnAction(e -> {
			consulta4(comboConsulta4.getValue());
		});
		
		btnConsulta5.setOnAction(e -> {
			consulta5(comboConsulta5.getValue());
		});
		
		btnConsulta7.setOnAction(e -> {
			Aeroporto origem = gerAero.buscarCodigo(textfieldConsulta7A.getText());
			Aeroporto destino = gerAero.buscarCodigo(textfieldConsulta7B.getText());
			consulta7(origem, destino);
		});

		pane.setCenter(mapkit);
		pane.setLeft(toolbox);

	}

	private void consulta1() {
		// Desenhar todos os aeroportos de um pa�s selecionado. Para selecionar o pa�s,
		// deve-se clicar perto de um aeroporto desse pa�s no mapa. Ex: se o usu�rio
		// clicar pr�ximo ao Aeroporto de Guarulhos, dever�o ser exibidos todos os
		// aeroportos brasileiros.
		if (gerenciador.getPosicao() == null) { // Pode ser feito com um try catch
			System.err.println("Nenhuma posi��o selecionada.");
			return;
		}
		
		Geo pos = new Geo(gerenciador.getPosicao().getLatitude(), gerenciador.getPosicao().getLongitude());
		Aeroporto maisProximo = gerAero.toArray().get(0);
		double geo_distancia = Geo.distancia(pos, maisProximo.getLocal());
		
		// inicializar no aero 0

		for (Aeroporto a : gerAero.toArray()) {
			Geo geo_aero = a.getLocal();
			double dist = Geo.distancia(pos, geo_aero);
			if (dist <= geo_distancia) {
				maisProximo = a;
				geo_distancia = dist;
			}
		}

		Pais pais = gerPaises.getPaises().get(maisProximo.getCodigoPais());
		//System.out.println("Pa�s Selecionado: " + pais);
		output.setText("Consulta 1\nPais Selecionado: " + pais +"\n\n");
		List<Aeroporto> aero_pais = pais.getAeroportos();

		List<MyWaypoint> lstPoints = new ArrayList<>();

		gerenciador.clear();
		
		for (Aeroporto a : aero_pais) {
			Tracado tr = new Tracado();
			tr.setLabel(a.getNome() + "(" + a.getCodigo() + ")");
			tr.setWidth(3);
			tr.setCor(new Color(0, 0, 0, 60));
			tr.addPonto(a.getLocal());
			lstPoints.add(new MyWaypoint(Color.RED, a.getNome() + " (" + a.getCodigo() + ")", a.getLocal(), 6));
			output.setText(output.getText() + a + "\n");
		}
		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint(); // Redesenha o mapa
	}
	
	private void consulta2() {
		//Desenhar todas as rotas que saem ou chegam em um pa�s selecionado. A sele��o deve ser realizada como na consulta 1.
		
		Geo pos = new Geo(gerenciador.getPosicao().getLatitude(), gerenciador.getPosicao().getLongitude());
		Aeroporto maisProximo = gerAero.toArray().get(0);
		double geo_distancia = Geo.distancia(pos, maisProximo.getLocal());
		
		// inicializar no aero 0

		for (Aeroporto a : gerAero.toArray()) {
			Geo geo_aero = a.getLocal();
			double dist = Geo.distancia(pos, geo_aero);
			if (dist <= geo_distancia) {
				maisProximo = a;
				geo_distancia = dist;
			}
		}

		Pais pais = gerPaises.getPaises().get(maisProximo.getCodigoPais());
		output.setText("Consulta 2\nPais Selecionado: " + pais +"\n\n");

		List<Aeroporto> aero_pais = pais.getAeroportos();
		
		Tracado tr = new Tracado();
		List<MyWaypoint> lstPoints = new ArrayList<>();
		gerenciador.clear();

		for (Rota r : gerRotas.toArray()) {
			if(r.getOrigem().getCodigoPais().equals(pais.getCodigo()) || r.getDestino().getCodigoPais().equals(pais.getCodigo())) {
				tr = new Tracado();
				// Origem
				tr.addPonto(r.getOrigem().getLocal());
				tr.setLabel(r.getOrigem().getNome() + "(" + r.getOrigem().getCodigo() + ")");
				
				// Destino
				tr.addPonto(r.getDestino().getLocal());
				tr.setLabel(r.getDestino().getNome() + "(" + r.getDestino().getCodigo() + ")");
				
				// Inicia Cores Basicas
				Color corOrigem = Color.RED;
				Color corDestino = Color.RED;
				
				// Altera cor se origem ou destino n�o forem o pais selecionado
				if(!r.getOrigem().getCodigoPais().equals(pais.getCodigo())) {
					corOrigem = Color.BLUE;
				} else { // remove da lista de aeroportos do pais para que seja adicionado um waypoint de outra cor depois
					aero_pais.remove(r.getOrigem());
					//System.out.println("Removido: " + r.getOrigem());
				}
				
				if(!r.getDestino().getCodigoPais().equals(pais.getCodigo())) {
					corDestino = Color.BLUE;
				} else {
					aero_pais.remove(r.getDestino());
					//System.out.println("Removido: " + r.getDestino());
				}
				
				// Cria waypoints e depois adiciona para lista
				MyWaypoint origem = new MyWaypoint(corOrigem, r.getOrigem().getNome() + "(" + r.getOrigem().getCodigo() + ")", r.getOrigem().getLocal(), 6);
				MyWaypoint destino = new MyWaypoint(corDestino, r.getDestino().getNome() + "(" + r.getDestino().getCodigo() + ")", r.getDestino().getLocal(), 6);
				lstPoints.add(origem);
				lstPoints.add(destino);
				output.setText(output.getText() + r + "\n");
				gerenciador.addTracado(tr);
			} 
		}
		
		for (Aeroporto a : aero_pais) {
			tr = new Tracado();
			tr.setLabel(a.getNome());
			tr.addPonto(a.getLocal());
			lstPoints.add(new MyWaypoint(Color.PINK, a.getNome() + " (" + a.getCodigo() + ")", a.getLocal(), 6));
		}
		
		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint();
	}
	
	private void consulta3(Double value) {
		
		// A partir de uma dist�ncia m�xima (em km) e da sele��o de um aeroporto, desenhar todas as rotas onde ele � a origem. 
		// A sele��o de dist�ncia pode ser feita atrav�s de um campo de texto ou algum outro componente visual.
		
		if (gerenciador.getPosicao() == null) { // Pode ser feito com um try catch
			System.err.println(".");
			return;
		}
		
		double distancia = value;
		output.setText("Consulta 3 \nPar�metro de dist�ncia: " + value + " km\n\nV�os: \n");
		
		Geo pos = new Geo(gerenciador.getPosicao().getLatitude(), gerenciador.getPosicao().getLongitude());
		Aeroporto maisProximo = gerAero.toArray().get(0);
		double geo_distancia = Geo.distancia(pos, maisProximo.getLocal());


		for (Aeroporto a : gerAero.toArray()) {
			Geo geo_aero = a.getLocal();
			double dist = Geo.distancia(pos, geo_aero);
			if (dist <= geo_distancia) {
				maisProximo = a;
				geo_distancia = dist;
			}
		}
		
		List<Rota> rotas = new ArrayList<>();
		
		for (Rota r : gerRotas.toArray()) {
			if(r.getOrigem().equals(maisProximo)) {
				rotas.add(r);
			}
		}

		Pais pais = gerPaises.getPaises().get(maisProximo.getCodigoPais());
		//System.out.println("Pais Selecionado: " + pais);
		//System.out.println(maisProximo);
		
		Tracado tr = new Tracado();
		List<MyWaypoint> lstPoints = new ArrayList<>();
		gerenciador.clear();
		
		for (Rota r : rotas) {
			if(r.getOrigem().getLocal().distancia(r.getDestino().getLocal()) <= distancia) {
				tr = new Tracado();
				tr.addPonto(r.getOrigem().getLocal());
				tr.addPonto(r.getDestino().getLocal());
				lstPoints.add(new MyWaypoint(Color.RED, r.getDestino().toString(), r.getOrigem().getLocal(), 6));
				lstPoints.add(new MyWaypoint(Color.BLUE, r.getOrigem().toString(), r.getDestino().getLocal(), 6));
				gerenciador.addTracado(tr);
				output.setText(output.getText() + r + "\n");
			}
		}
		
		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint();
	}
	
	private void consulta4(Pais selecionado) {
		//Exibir no mapa os 10 aeroportos com maior volume de tr�fego (a n�vel mundial ou de um pa�s espec�fico). 
		//O volume de tr�fego � calculado contando-se todas as rotas que chegam ou saem de cada aeroporto. 
		//Para selecionar o pa�s, use uma caixa de texto ou outro componente adequado. 
		//Deve-se explorar o tamanho e/ou cor do ponto correspondente, para destacar aeroportos com maior ou menor tr�fego.
		output.setText("Consulta 4 \n\n");
		Map<Aeroporto, Integer> trafego = new HashMap<Aeroporto, Integer>();
		for(Aeroporto a : selecionado.getAeroportos()) {
			trafego.put(a, 0);
		}
		
		// Verifica lista de rotas origem e destinos e incrementa no contador (hashmap)
		for(Rota r : gerRotas.toArray()) {
			Aeroporto origem = r.getOrigem();
			Aeroporto destino = r.getDestino();
			
			if (trafego.containsKey(origem)) 
			{
				Integer value = trafego.get(origem);
				if (value == null) {
					value = 0;
				}
				value++;
				trafego.put(origem, value);
			}
			
			if (trafego.containsKey(destino)) 
			{
				Integer value = trafego.get(destino);
				if (value == null) {
					value = 0;
				}
				value++;
				trafego.put(destino, value);
			}
		}

		
		List<Entry<Aeroporto, Integer>> result = trafego.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.limit(10)
				.collect(Collectors.toList());
		
		
		List<MyWaypoint> lstPoints = new ArrayList<>();
		gerenciador.clear();
		
		int trafegoTotalResultado = 0;
		
		for(Entry<Aeroporto, Integer> entry : result) {
			trafegoTotalResultado += entry.getValue();
		}
		
		for(Entry<Aeroporto, Integer> entry : result) {
			Aeroporto a = entry.getKey();
			int count = entry.getValue();
			int tamanho;
			Color waypointColor = Color.RED;
			if(trafegoTotalResultado != 0 && count != 0) { // evitar divis�o por 0
				tamanho = (count*60)/(6*350);
				//System.out.println("Tam " + tamanho);
			} else {
				tamanho = 6;
				waypointColor = Color.BLUE;
			}
			lstPoints.add(new MyWaypoint(waypointColor, a.getNome() + " (" + count + " voos)", a.getLocal(), tamanho));
			output.setText(output.getText() + count + " voos  (" + entry.getKey().getCodigo() + ") - " + entry.getKey().getNome() + "\n");
		}
		
		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint();
		//System.out.println(result);
	}
	
	private void consulta5(Aeronave aviao) {
		// Selecionar uma aeronave espec�fica e exibir todas as rotas onde a aeronave � utilizada.
		if(aviao == null) return;
		List<MyWaypoint> lstPoints = new ArrayList<>();
		output.setText("Consulta 5 \nAeronave (" + aviao.getCodigo() + "): " + aviao.getDescricao() + "\n\n");
		
		gerenciador.clear();
		Tracado tr = new Tracado();
		for(Rota r : gerRotas.toArray()) {
			if(r.getAeronave() == aviao) {
				tr = new Tracado();
				Aeroporto origem = r.getOrigem();
				Aeroporto destino = r.getDestino();
				tr.setLabel(origem.getNome() + "at�" + destino.getNome());
				lstPoints.add(new MyWaypoint(Color.RED, origem.getNome(), origem.getLocal(), 6));
				lstPoints.add(new MyWaypoint(Color.RED, destino.getNome(), destino.getLocal(), 6));
				tr.addPonto(r.getOrigem().getLocal());
				tr.addPonto(r.getDestino().getLocal());
				gerenciador.addTracado(tr);
				output.setText(output.getText() + r + "\n");
			}
		}
		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint();
	}
	
	private void consulta7(Aeroporto origem, Aeroporto destino) {
		if(origem == null || destino == null) return;
		output.setText("Consulta 7 \n\n");	
		List<MyWaypoint> lstPoints = new ArrayList<>();
		Tracado tr = new Tracado();
		
		gerenciador.clear();
		List<String> caminho = auxConsulta7grafo(origem, destino);
		ArrayList<Aeroporto> aeroportos_caminho = new ArrayList<Aeroporto>();
		
		for(int i = 0; i < caminho.size(); i++) {
			Aeroporto a = gerAero.buscarCodigo(caminho.get(i));
			aeroportos_caminho.add(a);
			lstPoints.add(new MyWaypoint(Color.RED, a.getNome() + " (" + a.getCodigo() + ")", a.getLocal(), 6));
		}
		
		double distancia_total = 0;
		
		for(int i = 0; i < aeroportos_caminho.size() -1; i++) {
			tr = new Tracado();
			Aeroporto o = aeroportos_caminho.get(i);
			Aeroporto d = aeroportos_caminho.get(i + 1);
			double calc_dist = Geo.distancia(o.getLocal(), d.getLocal());
			distancia_total += calc_dist;
			//System.out.println(o + " -> " + d);
			tr.setLabel(o.getNome() + "ate" + d.getNome());
			tr.addPonto(o.getLocal());
			tr.addPonto(d.getLocal());
			gerenciador.addTracado(tr);
			output.setText(output.getText() + o.getCodigo() + " -> " + d.getCodigo() + " (Distancia: " + String.format("%.2f", calc_dist) + " km)\n");
		}
		output.setText(output.getText() + "Distancia total: " + String.format("%.2f", distancia_total) + " km\n");
		output.setText(output.getText() + "\nAlgoritmo de Diijkstra");
		
		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint();
	}
	
private List<String> auxConsulta7grafo(Aeroporto origem, Aeroporto destino){
		
		ArrayList<String> caminho = new ArrayList<String>();
		
		String cod_origem = origem.getCodigo();
		String cod_destino = destino.getCodigo();
		
		Map<String, Vertex> nodes = new HashMap<String, Vertex>();
		List<Edge> edges = new ArrayList<Edge>();
		
		for(Aeroporto a : gerAero.toArray()) {
			nodes.put(a.getCodigo(), new Vertex(a.getCodigo(), a.getNome()));
		} // interessante mover isto pra outra parte do c�digo para nao ter que fazer de novo e de novo
		
		int unique_id = 0;
		double weigth = 0;
		
		for(Rota r : gerRotas.toArray()) {
			Aeroporto o = r.getOrigem();
			Aeroporto d = r.getDestino();
			weigth = Geo.distancia(o.getLocal(), d.getLocal());
			edges.add(new Edge(o.getCodigo() + unique_id + d.getCodigo(), nodes.get(o.getCodigo()), nodes.get(d.getCodigo()), (int) weigth));
			unique_id++;
		}
		
		List<Vertex> nodes_list = nodes.values().stream().collect(Collectors.toList());
		
		Graph graph = new Graph(nodes_list, edges);
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
		dijkstra.execute(nodes.get(cod_origem));
		LinkedList<Vertex> path = dijkstra.getPath(nodes.get(cod_destino));
		
		if(path != null && path.size() > 0) {
			for(Vertex vertex : path) {
				caminho.add(vertex.getId());
				//System.out.println(vertex.getId() + " - " + vertex.getName() );
			}
		}
		
		return caminho;
	}

	private class EventosMouse extends MouseAdapter {
		private int lastButton = -1;

		@Override
		public void mousePressed(MouseEvent e) {
			JXMapViewer mapa = gerenciador.getMapKit().getMainMap();
			GeoPosition loc = mapa.convertPointToGeoPosition(e.getPoint());
			// System.out.println(loc.getLatitude()+", "+loc.getLongitude());
			lastButton = e.getButton();
			// Botão 3: seleciona localização
			if (lastButton == MouseEvent.BUTTON3) {
				gerenciador.setPosicao(loc);
				gerenciador.getMapKit().repaint();
			}
		}
	}

	private void createSwingContent(final SwingNode swingNode) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				swingNode.setContent(gerenciador.getMapKit());
			}
		});
	}

	public static void main(String[] args) {
		launch(args);
	}
}
